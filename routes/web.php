<?php

use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\DashboardPostsController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\RegisterController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Models\Posts;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home",
        "active" => "home"
    ]);
});

Route::get('/about', function () {
    return view('about', [
        "title" => "About",
        "active" => "about",
        "name" => "Aditya Forest",
        "email" => "adityaforestresananta@gmail.com",
        "image" => "adityaforest.jpg"
    ]);
});

Route::get('/posts', [PostsController::class,'index']);

//single post page
Route::get('/posts/{posts:slug}' , [PostsController::class,'show']);

Route::get('/categories' , function(){
    return view('categories' , [
        'title' => 'Post Categories',      
        "active" => "categories",  
        'categories' => Category::all()
    ]);
});

// Route::get('/categories/{category:slug}' , function(Category $category){
//     return view('posts' , [
//         'title' => "Posts by Category : $category->name ",
//         "active" => "posts",
//         'posts' => $category->posts->load('category','author'),        
//     ]);
// });

// Route::get('/authors/{author:username}' , function(User $author) {
//     return view('posts' , [
//         'title' => "Posts by Author : $author->name ",
//         "active" => "posts",
//         'posts' => $author->posts->load('category','author'),        
//     ]);
// });

Route::get('/login' , [LoginController::class , 'index'])->name('login')->middleware('guest');
Route::post('/login' , [LoginController::class , 'authenticate']);
Route::post('/logout' , [LoginController::class , 'logout']);

Route::get('/register' , [RegisterController::class , 'index'])->middleware('guest');
Route::post('/register' , [RegisterController::class , 'store']);

Route::get('/dashboard' , function(){
    return view('dashboard.index');
})->middleware('auth');

Route::get('/dashboard/posts/createSlug' , [DashboardPostsController::class , 'createSlug'])->middleware('auth');

Route::resource('/dashboard/posts' , DashboardPostsController::class)->middleware('auth');
// Route::resource('/dashboard/posts' , '\App\Http\Controllers\DashboardPostsController')->middleware('auth');

Route::resource('/dashboard/categories' , AdminCategoryController::class)->except('show')->middleware('isAdmin');

