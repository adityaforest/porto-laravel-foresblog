{{-- @dd($post) --}}

@extends('layouts.main')

@section('page-container')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">

                <h2 class="mb-3">{{ $post->title }}</h2>
                <p>
                    By.
                    <a href="/posts?author={{ $post->author->username }}" class="text-decoration-none">
                        {{ $post->author->name }}
                    </a>
                    in
                    <a href="/posts?categpry={{ $post->category->slug }}" class="text-decoration-none">
                        {{ $post->category->name }}
                    </a>
                    {{ $post->created_at->diffForHumans() }}
                </p>

                @if ($post->image)
                    <div class="" >
                        <img src="{{ asset('storage/' . $post->image) }}" alt="{{ $post->category->name }}"
                            class="img-fluid" style="width:100%;">
                    </div>
                @else
                    <img src="https://source.unsplash.com/1200x400?{{ $post->category->name }}" class="card-img-top"
                        alt="{{ $post->category->name }}">
                @endif


                <article class="my-3 fs-5">
                    {{-- biar kalo ada tag html di kontennya tetap kecetak , pake {!!  xxxx !!} --}}
                    {!! $post->body !!}
                </article>

                <a href="/posts" class="d-block mt-3">Back to Posts</a>
            </div>
        </div>
    </div>
@endsection
