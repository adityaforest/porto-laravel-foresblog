<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Posts;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Aditya Forest',
            'username' => 'adityaforest' ,
            'email' => 'adityaforestresananta@gmail.com',
            'password' => bcrypt('password')                
        ]);
        
        \App\Models\User::factory(3)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


            // User::create([
            //     'name' => 'User baru',
            //     'email' => 'userbaru@gmail.com',
            //     'password' => bcrypt('12345')                
            // ]);

            Category::create([
                'name' => 'Web Programming',
                'slug' => 'web-programming'
            ]);

            Category::create([
                'name' => 'Web Design',
                'slug' => 'web-design'
            ]);
            
            Category::create([
                'name' => 'Personal',
                'slug' => 'personal'
            ]);

            Posts::factory(20)->create();
            
            // Posts::create([
            //     'title' => 'Judul Pertama',
            //     'slug' => 'judul-pertama',
            //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a vero voluptas optio consectetur culpa, facere,',
            //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat repellat nam amet et veritatis earum aut
            //     dolorem porro officia molestias, hic, exercitationem quae nulla! </p>
            //     <p>Cum repellendus necessitatibus, aspernatur quam eum consectetur adipisci. Quo, repellat enim, sapiente, fugiat atque
            //     eum nam illo ipsam sint quod odit dolores consequuntur eos provident reiciendis nobis accusantium aperiam similique
            //     amet soluta voluptatum odio. Quos, neque consequatur! Doloremque ipsa consequuntur natus, accusamus eum eius aperiam
            //     rerum perspiciatis voluptatibus, reprehenderit harum earum veniam dolores quo aliquid dolorum.</p>',
            //     'category_id' => 1,
            //     'user_id' => 1                
            // ]);

            // Posts::create([
            //     'title' => 'Judul Kedua',
            //     'slug' => 'judul-kedua',
            //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a vero voluptas optio consectetur culpa, facere,',
            //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat repellat nam amet et veritatis earum aut
            //     dolorem porro officia molestias, hic, exercitationem quae nulla! </p>
            //     <p>Cum repellendus necessitatibus, aspernatur quam eum consectetur adipisci. Quo, repellat enim, sapiente, fugiat atque
            //     eum nam illo ipsam sint quod odit dolores consequuntur eos provident reiciendis nobis accusantium aperiam similique
            //     amet soluta voluptatum odio. Quos, neque consequatur! Doloremque ipsa consequuntur natus, accusamus eum eius aperiam
            //     rerum perspiciatis voluptatibus, reprehenderit harum earum veniam dolores quo aliquid dolorum.</p>',
            //     'category_id' => 2,
            //     'user_id' => 1                
            // ]);

            // Posts::create([
            //     'title' => 'Judul Ketiga',
            //     'slug' => 'judul-ketiga',
            //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a vero voluptas optio consectetur culpa, facere,',
            //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat repellat nam amet et veritatis earum aut
            //     dolorem porro officia molestias, hic, exercitationem quae nulla! </p>
            //     <p>Cum repellendus necessitatibus, aspernatur quam eum consectetur adipisci. Quo, repellat enim, sapiente, fugiat atque
            //     eum nam illo ipsam sint quod odit dolores consequuntur eos provident reiciendis nobis accusantium aperiam similique
            //     amet soluta voluptatum odio. Quos, neque consequatur! Doloremque ipsa consequuntur natus, accusamus eum eius aperiam
            //     rerum perspiciatis voluptatibus, reprehenderit harum earum veniam dolores quo aliquid dolorum.</p>',
            //     'category_id' => 2,
            //     'user_id' => 2                
            // ]);

            // Posts::create([
            //     'title' => 'Judul Keempat',
            //     'slug' => 'judul-keempat',
            //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis a vero voluptas optio consectetur culpa, facere,',
            //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat repellat nam amet et veritatis earum aut
            //     dolorem porro officia molestias, hic, exercitationem quae nulla! </p>
            //     <p>Cum repellendus necessitatibus, aspernatur quam eum consectetur adipisci. Quo, repellat enim, sapiente, fugiat atque
            //     eum nam illo ipsam sint quod odit dolores consequuntur eos provident reiciendis nobis accusantium aperiam similique
            //     amet soluta voluptatum odio. Quos, neque consequatur! Doloremque ipsa consequuntur natus, accusamus eum eius aperiam
            //     rerum perspiciatis voluptatibus, reprehenderit harum earum veniam dolores quo aliquid dolorum.</p>',
            //     'category_id' => 2,
            //     'user_id' => 2                
            // ]);
    }
}
