<?php

namespace App\Models;

class Posts
{
    private static $blog_posts = [
        [
            "title" => "JUDUL PERTAMA",
            "slug" => "judul-pertama",
            "author" => "author pertama",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit fugit dignissimos quae quisquam sunt? Facere ratione
            ducimus molestias deserunt obcaecati ipsam eos necessitatibus ipsum repellendus fugit similique debitis illum possimus
            cupiditate temporibus veritatis nihil ad, voluptas quidem accusamus nesciunt. Autem consequatur magni accusantium
            incidunt aut inventore reprehenderit natus? Assumenda, dolor magnam odit voluptatem sed aperiam vero voluptas nobis
            impedit? Rerum maxime doloribus autem obcaecati numquam, veritatis, sit illum quaerat ratione quidem facere. Dolorum
            quia magni et, fugiat expedita eum placeat!"
        ],
        [
            "title" => "JUDUL kedua",
            "slug" => "judul-kedua",
            "author" => "author kedua",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit fugit dignissimos quae quisquam sunt? Facere ratione
            ducimus molestias deserunt obcaecati ipsam eos necessitatibus ipsum repellendus fugit similique debitis illum possimus
            cupiditate temporibus veritatis nihil ad, voluptas quidem accusamus nesciunt. Autem consequatur magni accusantium
            incidunt aut inventore reprehenderit natus? Assumenda, dolor magnam odit voluptatem sed aperiam vero voluptas nobis
            impedit? Rerum maxime doloribus autem obcaecati numquam, veritatis, sit illum quaerat ratione quidem facere. Dolorum
            quia magni et, fugiat expedita eum placeat!"
        ]
    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();       
        return $posts->firstWhere('slug',$slug);
    }
}
