<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //pagination pake punyanya bootstrap bukan defaultnya tailwind
        Paginator::useBootstrap();

        //gate 'admin' , ngecek tabel database user kolom is_admin true apa false
        Gate::define('admin' , function(User $user){
            return $user->is_admin;
        });
    }
}
